<?php

@session_start();
class main
{
    public static $DBH;
    public  static $user_id;
    public static $typed = [
        1 => 'Money',
        2 => 'Bonus',
        3 => 'Item'
    ];
    public static $rate = 1.25;

    public static function setup(){
        include 'config.php';

        self::$DBH = new PDO($dsn, $user, $pass, $opt);

        if (isset($_SESSION['logged_user']) && $_SESSION['logged_user']){
            self::$user_id = $_SESSION['logged_user']['id'];
        }

        return self::$DBH;
    }

    public static function serializeArray($array){
        $arr = [];
        foreach ($array as $key => $value) {
            $arr[$key] = htmlspecialchars($value);

        }
        return $arr;
    }

}