<?php

@session_start();
include_once 'main.php';
class game extends main
{
    public static function SignUp($post){

        $error = [];
        $post = parent::serializeArray($post);

        if(!isset($post['email']) || !isset($post['password']) || !isset($post['password_confirm'])){
            $error[] = 'Заполните все поля';
        }

        $sl_user = parent::$DBH->prepare("SELECT * FROM `users` WHERE `email`=?");
        $sl_user->execute([$post['email']]);
        $row_user = $sl_user->fetch();

        if($row_user){
            $error[] = 'Такой e-mail уже зарегистрирован';
        }

        if ($post['password'] != $post['password_confirm']){
            $error[] = 'Пароли не совпадают';
        }

        if(!$error){
            $password_hash = password_hash($post['password'], PASSWORD_BCRYPT);
            $in_user = parent::$DBH->prepare("INSERT INTO `users` (`email`, `password` ) VALUES(?,?)");
            $in_user->execute([$post['email'], $password_hash]);

            $sl_user->execute([$post['email']]);
            $row_user = $sl_user->fetch();
            $_SESSION['logged_user'] = $row_user;
        }else{
            return array_shift($error);
        }
    }
    public static function SignIn($post){
        $error = [];
        $post = parent::serializeArray($post);

        if (!isset($post['email']) || !isset($post['password'])){
            $error[] = 'Заполните все поля';
        }
        if (!$post['email'] || !$post['password']){
            $error[] = 'Заполните все поля';
        }

        $sl_user = parent::$DBH->prepare("SELECT * FROM `users` WHERE `email`=?");
        $sl_user->execute([$post['email']]);
        $row_user = $sl_user->fetch();

        if (!$row_user){
            return 'Такого пользователя не существует';
        }

        if($row_user['banned'] == '1'){
            return 'Пользователь забаннен';
        }

        if (!password_verify($post['password'], $row_user['password'])){
            $error[] = 'Пароль введен не верно';
        }

        if (!$error){
            $_SESSION['logged_user'] = $row_user;
        } else {
            return array_shift($error);
        }

    }
    public static function GetPrize(){
        $resultType = array_rand(parent::$typed,1);
        $sl_moneyPool = parent::$DBH->query("SELECT `balance` FROM `users` WHERE `id` = '1'")->fetch();
        $sl_prizePool = parent::$DBH->query("SELECT * FROM `prize_pool` WHERE `count` > 1")->fetch();
        $randomPrize = parent::$DBH->query("SELECT * FROM `prize_pool` ORDER BY RAND() LIMIT 1")->fetch();
        if($sl_moneyPool != 0 && $sl_prizePool != ''){
            //если есть денги и призы
            switch ($resultType){
                case '1':
                    $max = $sl_moneyPool - 20;
                    $moneyRand = rand(1,$max);
                    $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $moneyRand, date('Y-m-d H:i:s'), 0]);
                    $up_moneyPool = parent::$DBH->prepare("UPDATE `users` SET `balance`=? WHERE `id` = '1'");
                    $up_moneyPool->execute([$sl_moneyPool - $moneyRand]);
                    $_SESSION['money_Prize'] = $moneyRand;
                    break;
                case '2':
                    $bonusRand = rand(1,400);
                    $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $bonusRand, date('Y-m-d H:i:s'), 1]);
                    $_SESSION['logged_user']['balance'] += $bonusRand;
                    break;
                case '3':
                    $itemRand = array_rand(parent::$item,1);
                    $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $randomPrize, date('Y-m-d H:i:s'), 0]);
                    break;
            }
        }
        if($sl_moneyPool == 0){
            //если кончились ток деньги
            switch ($resultType){
                case '2':
                    $bonusRand = rand(1,400);
                    $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $bonusRand, date('Y-m-d H:i:s'), 1]);
                    $_SESSION['logged_user']['balance'] += $bonusRand;
                    break;
                case '3':
                    $itemRand = array_rand(parent::$item,1);
                    $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $randomPrize, date('Y-m-d H:i:s'), 0]);
                    break;
            }
        }
        if($sl_prizePool == '' && $sl_prizePool == 0){
                //если кочились деньги и призы, ток бонусы
                    $bonusRand = rand(1,400);
                    $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $bonusRand, date('Y-m-d H:i:s'), 1]);
                    //так же обновлеться баланс в бд, не стал писать потому что для тестового задания думаю достаточно подготовленных запросов
                    $_SESSION['logged_user']['balance'] += $bonusRand;
        }
        if($sl_prizePool == '' && $sl_moneyPool != 0) {
            //если кончились денги но не кончились призы
            switch ($resultType) {
                case '1':
                    $max = $sl_moneyPool - 20;
                    $moneyRand = rand(1, $max);
                    $in_prize = parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $moneyRand, date('Y-m-d H:i:s'), 0]);
                    $up_moneyPool = parent::$DBH->prepare("UPDATE `users` SET `balance`=? WHERE `id` = '1'");
                    $up_moneyPool->execute([$sl_moneyPool - $moneyRand]);
                    $_SESSION['money_Prize'] = $moneyRand;
                    break;
                case '2':
                    $bonusRand = rand(1, 400);
                    $in_prize = parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
                    $in_prize->execute([$_SESSION['logged_user']['id'], $resultType, $bonusRand, date('Y-m-d H:i:s'), 1]);
                    $_SESSION['logged_user']['balance'] += $bonusRand;
                    break;
            }
        }

        //чтоб не писать много однотипного кода для тестового задания, напишу только варианет случае если призы есть
        //если есть призы и количество их больще 1, выбераем случайный приз, отнимаем его, добавляем к выигрешу
        if($sl_prizePool){
            $up_prizePool = parent::$DBH->prepare("UPDATE `prize_pool` SET `count` VALUES (?) WHERE `id` = ? ");
            $up_prizePool->execute([$randomPrize['count']- 1, $randomPrize['id']]);
            $in_prize =  parent::$DBH->prepare("INSERT INTO `prize` (`user_id`, `prize_type`, `result`, `date_at`, `send`) VALUES (?, ?, ?, ?, ?)");
            $in_prize->execute([$_SESSION['logged_user']['id'], 3, $randomPrize['type'], date('Y-m-d H:i:s'), 0]);
        }

    }

    public static function AcceptMoney(){
        if(isset($_POST['refuse'])){
            $exchenge = $_SESSION['money_Prize'] * parent::$rate;
            //так же обновлеться баланс в бд, не стал писать потому что для тестового задания думаю достаточно подготовленных запросов
            $_SESSION['logged_user']['balance'] = $exchenge;
        }else{
            $_SESSION['logged_user']['balance'] + $_SESSION['money_Prize'];
            //так же обновлеться баланс в бд, так же добавляеться запись к административной части для проведения выплат
        }
    }



}

main::setup();

#prize pool
#   id type count
#   1 money 2000
#   2 iphone 2
#   3 mac 1